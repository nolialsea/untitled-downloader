# Untitled File Upload and Download
Simple web server to upload and download encrypted files.

### Install
`mvn package` to compile the jar file  
`target/untitled-1.0-SNAPSHOT.jar db migrate configuration.yml` to create the database  
`target/untitled-1.0-SNAPSHOT.jar server configuration.yml` to start the application  
After booting, the website will be available at http://localhost:8080/client/uploadFile

### Description
Use this web page to upload and download files securely.  
Files are encrypted before upload, and decrypted after download, so at no point the server can read your uploaded files.  
