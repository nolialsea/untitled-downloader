package com.untitled;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(DropwizardExtensionsSupport.class)
public class MultiPartTest {

    public static final ResourceExtension resourceExtension = ResourceExtension.builder()
            .addProvider(MultiPartFeature.class)
            .addResource(new TestResource())
            .build();

    @Test
    public void testClientMultipart() {
        final FormDataMultiPart multiPart = new FormDataMultiPart()
                .field("test-data", "Hello Multipart");
        final String response = resourceExtension.target("/test")
                .register(MultiPartFeature.class)
                .request()
                .post(Entity.entity(multiPart, multiPart.getMediaType()), String.class);
        assertThat("Multipart contains correct response", response.equals("Hello Multipart"));
    }

    @Path("test")
    public static class TestResource {
        @POST
        @Consumes(MediaType.MULTIPART_FORM_DATA)
        public String post(@FormDataParam("test-data") String testData) {
            return testData;
        }
    }
}