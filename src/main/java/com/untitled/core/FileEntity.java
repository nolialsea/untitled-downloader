package com.untitled.core;

import javax.persistence.*;

@Entity
@Table(name = "file")
@NamedQueries(
        {
                @NamedQuery(
                        name = "com.untitled.core.FileEntity.findAll",
                        query = "SELECT f FROM FileEntity f"
                ),
                @NamedQuery(
                        name = "com.untitled.core.FileEntity.findByUuid",
                        query = "SELECT f FROM FileEntity f WHERE f.uuid LIKE :uuid"
                )
        })
public class FileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "filename", nullable = false)
    private String fileName;

    public FileEntity(String fileName, String uuid){
        this.fileName = fileName;
        this.uuid = uuid;
    }

    public FileEntity() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
