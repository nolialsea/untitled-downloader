package com.untitled.resources;

import com.untitled.core.FileEntity;
import com.untitled.db.FileDAO;
import com.untitled.utils.Utils;
import io.dropwizard.hibernate.UnitOfWork;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.UUID;

@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileResource.class);

    private final FileDAO fileDAO;
    private final String uploadFolder;

    public FileResource(FileDAO fileDAO, String uploadFolder) {
        this.fileDAO = fileDAO;
        this.uploadFolder = uploadFolder;
    }

    @GET
    @Path("/{uuid}")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile(@PathParam("uuid") String uuid) throws Exception {
        LOGGER.info("Getting file: " + uuid);
        Optional<FileEntity> file = fileDAO.findByUuid(uuid);

        if (file.isPresent()) {
            String fileName = uploadFolder + uuid;
            InputStream is = new FileInputStream(fileName);
            return Response.ok(is)
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.get().getFileName() + "\"")
                    .build();
        }

        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }

    @POST
    @UnitOfWork
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("fileName") String fileName) throws IOException {
        // creates folder if it doesn't exist
        File theDir = new File(uploadFolder);
        if (!theDir.exists()) {
            theDir.mkdirs();
        }

        FileEntity file = new FileEntity(fileName, UUID.randomUUID().toString());
        String uploadedFileLocation = uploadFolder + file.getUuid();
        // save uploaded file to new location
        Utils.writeToFile(uploadedInputStream, uploadedFileLocation);

        fileDAO.create(file);
        String output = file.getUuid();

        return Response.ok(output).build();
    }
}