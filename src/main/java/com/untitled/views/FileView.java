package com.untitled.views;

import io.dropwizard.views.View;

public class FileView extends View {
    public FileView() {
        super("/mustache/file.mustache");
    }
}