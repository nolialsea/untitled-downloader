package com.untitled.db;

import com.untitled.core.FileEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class FileDAO extends AbstractDAO<FileEntity> {
    public FileDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<FileEntity> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Optional<FileEntity> findByUuid(String uuid) {
        return namedTypedQuery("com.untitled.core.FileEntity.findByUuid")
                .setParameter("uuid", uuid)
                .uniqueResultOptional();
    }

    public FileEntity create(FileEntity fileEntity) {
        return persist(fileEntity);
    }

    public List<FileEntity> findAll() {
        return list(namedTypedQuery("com.untitled.core.FileEntity.findAll"));
    }
}
